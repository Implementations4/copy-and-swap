#include <iostream>


/*
Copy-and-swap
*/

/*без идиомы 
anyClass& operator=(anyClass& other)
    {
        // self assignment check
        if (this != &other)
        {
            delete ptr;
            size = other.size;
            ptr = size? new int[size]: nullptr;
            memmove(ptr, other.ptr, size*sizeof(int));
            return *this;
        }
    }





    а) Освобождение памяти, назначенной this->ptr
    б) Выделение новой памяти для this->ptr и копирование значений
    в) Возврат *this
*/
/*c идиомой в котрой реализована функция swap*/

class anyClass{
    private:
    int size;
    int *ptr;
    public:
    anyClass(int s=0):size(s),ptr(size ? new int[size]:nullptr){

    }
    void swap(anyClass& obj1, anyClass& obj2){
        std::swap(obj1.size,obj2.size);
        std::swap(obj1.ptr,obj2.ptr);
    }
    anyClass(anyClass& other){//без идиомы c COW
       ptr=other.ptr;
       size=other.size; 
    }
    

    anyClass& operator=(anyClass& other){
        swap(*this,other);
        return *this;
    }

    
};





int main()
{
  
   


return 0;
 
}

